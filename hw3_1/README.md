This directory contains a starter kit for Homework 3.1, "Regex highlighting
with ANTLR4".

The compiler comes in two parts: an ANTLR4 grammar in Regex.g4 which is used
to generate a lexer and parser, and a listener-based compiler in compiler.js.

An ANTLR4 runtime is provided for you in the antlr/ directory. As long as you
have a relatively recent JRE available on your system, you should be able to
use it seamlessly from npm.

To compiler your grammar, run `npm run antlr4`. Then run `npm run build` to package
your compiler with browserify; you can open index.html in a browser to interact
with it. There are no automated tests, but you should verify that it
works on the regex that is initially provided. The initial output (below the
compile! button, but before you click it) comes with working highlighting, except
for the colors.

You can also use ANTLR's java backend to inspect your grammar more directly.
This requires a working JDK on your system (javac needs to be available). First,
run `npm run antlr4-java` to compile the grammar. You can then view a parse tree
with the grun.sh script (or grun.bat on windows).

grun takes 3 arguments: the name of the grammar, the start rule, and the method
to use to display the tree. It then waits for input on standard in.

For example,

```
$ ./grun.sh Regex re -tree
abc
Ctrl-D
```

should print out the tree in lisp notation

```
(re (expression (element (atom a)) (element (atom b)) (element (atom c))) <EOF>)
```

Ctrl-D closes standard in on linux; Ctrl-Z will do it in a windows CMD.

You can also use the -gui option instead of -tree to bring up a pretty graphical
representation in a window. If you've typed out the regex you want to parse into
a file called foo.txt, you could view its tree with:

```
$ ./grun.sh Regex re -gui < foo.txt
```
