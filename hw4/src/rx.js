class Stream {
    constructor() {
        this.callbacks = [];
    }

    // Add Stream methods here.



    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            jsonp: "callback",
            dataType: "jsonp",
            data: _data,
            success: function(data) { out._push(data[1]); }
        });
        return out;
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    // Add your hooks to implement Parts 2-4 here.
}
