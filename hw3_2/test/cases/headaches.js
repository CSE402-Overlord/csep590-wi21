function sum(ctx, ...args) {
    return args.reduce((a, x) => a + x);
}

exports.helpers = [
    ["sum", sum],
];
exports.blockHelpers = [];
exports.ctx = {};
exports.description = "Why parsing numbers is hard (YOU DON'T HAVE TO PASS THIS TEST)";
