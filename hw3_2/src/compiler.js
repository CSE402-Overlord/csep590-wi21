const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._bodySource = `var ${this._outputVar} = "";\n`;

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this._bodySource += `return ${this._outputVar};\n`;
        return new Function(this._inputVar, this._bodySource);
    }

    append(expr) {
        this._bodySource += `${this._outputVar} += ${expr};\n`;
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
