This directory contains a starter kit for Homework 1, "Regular Expressions".

`engexp` is a Node.js module, written in [TypeScript](http://www.typescriptlang.org/), a typed
superset of JavaScript. The repository initially contains TypeScript original sources (\*.ts);
generate the executable JavaScript files (\*.js) with the TypeScript compiler `tsc`. The module
is configured to run the compiler for you automatically before you run the tests.

To get started after cloning this repository, install [Node.js](https://nodejs.org),
and run `npm install` in this directory (hw1/). After NPM successfully resolves all
dependencies into `npm_modules`, run `npm test`. You should see an output similar to the following:

```
> hw1@2021.1.0 pretest /Users/clwang/Courses/CSEP-590B-wi/csep590-wi21/hw1
> tsc


> hw1@2021.1.0 test /Users/clwang/Courses/CSEP-590B-wi/csep590-wi21/hw1
> mocha --require source-map-support/register



  custom
    ✓ put description of test 1 here

  EngExp
    ✓ should handle invalid and duplicated flags
    1) should parse a basic URL
    ✓ should parse a URL with query and fragment
    2) match, then, zeroOrMore, oneOrMore, optional, and maybe should agree for string repeated exactly once
    3) should parse a disjunctive date pattern
    4) should correctly parenthesize disjunctions
    5) disjunctive date pattern with levels
    6) should capture nested groups
    7) should capture nested groups and work with disjunctions


  3 passing (28ms)
  7 failing

  1) EngExp
       should parse a basic URL:

      AssertionError: expected true to be false
      + expected - actual

      -true
      +false
      
      at Context.it (test/engexp.ts:33:54)

  2) EngExp
       match, then, zeroOrMore, oneOrMore, optional, and maybe should agree for string repeated exactly once:

      AssertionError: expected false to be true
      + expected - actual

      -false
      +true
      
      at Context.it (test/engexp.ts:127:40)

  3) EngExp
       should parse a disjunctive date pattern:
     Error: unimplemented
      at EngExp.or (src/engexp.ts:126:15)
      at Context.it (test/engexp.ts:133:27)

  4) EngExp
       should correctly parenthesize disjunctions:
     Error: unimplemented
      at EngExp.or (src/engexp.ts:126:15)
      at Context.it (test/engexp.ts:165:14)

  5) EngExp
       disjunctive date pattern with levels:
     Error: unimplemented
      at EngExp.or (src/engexp.ts:126:15)
      at Context.it (test/engexp.ts:177:27)

  6) EngExp
       should capture nested groups:
     Error: unimplemented
      at EngExp.beginCapture (src/engexp.ts:131:15)
      at Context.it (test/engexp.ts:210:14)

  7) EngExp
       should capture nested groups and work with disjunctions:
     Error: unimplemented
      at EngExp.beginLevel (src/engexp.ts:141:15)
      at Context.it (test/engexp.ts:225:28)
```

Out of the nine tests defined in [test/engexp.ts](test/engexp.ts), only two are
currently passing. The first two failures are due to a bug in the starter kit - you should
fix that first to familiarize yourself with the code. Once you've done that, your goal is
to implement missing functions `or`, `beginLevel`, `endLevel`, `beginCapture`, and `endCapture`
in [src/engexp.ts](src/engexp.ts) so that the rest of the tests pass. You should also write
your own tests, in [test/custom.ts](test/custom.ts).

Note: we will be verifying your solution on an additional hidden suite of tests, so
please make sure your implementation is general-purpose.
